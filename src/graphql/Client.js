/* global graphcoolApiLink */

import ApolloClient, { createNetworkInterface } from 'apollo-client'

const client = new ApolloClient({
  networkInterface: createNetworkInterface({
    uri: graphcoolApiLink,
  }),
})

export default client
