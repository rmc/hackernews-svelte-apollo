const resolve = require('path').resolve
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = env => ({
  context: resolve('./src'),
  entry: './main.js',
  output: {
    path: resolve('./dist'),
    filename: 'bundle.js',
  },
  module: {
    loaders: [
      {
        test: /\.html$/,
        exclude: /node_modules/,
        loader: 'svelte-loader',
      },
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader',
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Hackernews - Welcome!',
      filename: 'index.html',
    }),
    new webpack.DefinePlugin({
      graphcoolApiLink: JSON.stringify(env.graphcoolApiLink),
    }),
  ],
})
